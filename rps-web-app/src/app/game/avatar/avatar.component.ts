import { flatten } from '@angular/compiler';
import { Component, Input, OnInit } from '@angular/core';
import { Choice } from '../../helper/Choice';

@Component({
  selector: 'app-avatar',
  templateUrl: './avatar.component.html',
  styleUrls: ['./avatar.component.scss']
})
export class AvatarComponent implements OnInit {

  
  _name: string = "Günther";
  get name(): string {
      return this._name;
  }
  @Input() set name(value: string) {
      this._name = value;
      this.setAvatarImage();
  }



  avatarImage: string = "https://avatars.dicebear.com/4.5/api/human/test.svg";

  @Input()
  choice: Choice = Choice.Scissors;

  choiceImage: string = "";

  @Input()
  ready: boolean = false;

  @Input()
  showChoice: boolean = false;

  @Input()
  largeSize: boolean = false;

  @Input()
  result: string = "";

  


  constructor() { }

  ngOnInit(): void {
    this.setAvatarImage();
    this.setChoice();
  }

  setAvatarImage(){
    this.avatarImage = "https://avatars.dicebear.com/4.5/api/human/" + this.name + ".svg";
  }

  setChoice(){
    switch(this.choice) { 
      case Choice.Rock: { 
        this.choiceImage = "assets/images/rock.png";
        break; 
      } 
      case Choice.Paper: { 
        this.choiceImage = "assets/images/paper.png";
        break; 
      } 
      case Choice.Scissors: { 
        this.choiceImage = "assets/images/scissors.png";
        break; 
      } 
      default: { 
        this.choiceImage = "assets/images/rock.png";
        break; 
      } 
   } 
  }

}
