import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GameComponent } from './game.component';
import { AvatarComponent } from './avatar/avatar.component';
import { UiElementsModule } from '../ui-elements/ui-elements.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import {ClipboardModule} from '@angular/cdk/clipboard'; 

@NgModule({
  declarations: [GameComponent, AvatarComponent],
  imports: [
    CommonModule,
    UiElementsModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    ClipboardModule
  ]
})
export class GameModule { }
