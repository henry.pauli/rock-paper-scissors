import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Choice } from '../helper/Choice';
import { Game } from '../models/game.model';
import { Player } from '../models/player.model';
import { GameService } from '../services/game.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss']
})
export class GameComponent implements OnInit {

  link: string = "game";
  name: string = "";
  joinForm: FormGroup;

  rock: string = Choice.Rock;
  paper: Choice = Choice.Paper;
  scissors: Choice = Choice.Scissors;

  inGame: boolean = true;
  showChoice: boolean = false;

  isChoiceSet: boolean = false;

  game: Game = new Game();
  players: Player[] = [];

  // game id
  get gameId(): string {
    return sessionStorage.getItem('gameId');;
  }
  set gameId(value: string) {
    sessionStorage.setItem('gameId', value);
  }

  // player id
  get playerId(): string {
    return sessionStorage.getItem('playerId');
  }
  set playerId(value: string) {
    sessionStorage.setItem('playerId', value);
  }

  // all players data for the game
  players$: Observable<Player[]>;
  game$: Observable<Game>;

  // countdown
  countdown: boolean = false;
  countdownCounter: string = "";

  constructor(
    public fb: FormBuilder, 
    public gameService: GameService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {

    var gameIdFromRoute: string = this.route.snapshot.paramMap.get('id');
    var playerIdIsInStorage: boolean = (this.playerId != null) && (this.playerId != "");
    var gameIdIsCorrect: boolean = gameIdFromRoute == this.gameId;


    if(playerIdIsInStorage && gameIdIsCorrect){
      // init players data
      this.players$ = this.gameService.getPlayersData(this.gameId);
      this.players$.subscribe(event =>{
        this.players = event;
      });
      // init game data
      this.game$ = this.gameService.getGameData(this.gameId);
      this.game$.subscribe(event =>{
        this.game = event;
      });
      // set in game
      this.inGame = true;
    }
    else{
      this.gameId = gameIdFromRoute;
      this.inGame = false;
      this.initJoinForm();
    }
  }

  async setChoice(choice: string){
    this.gameService.updatePlayerChoice(this.gameId, this.playerId, choice);
    this.isChoiceSet = true;
  }

  async startGame(){
    var result = this.gameService.startGame(this.gameId);

   //countdown
    this.countdown = true;
    this.countdownCounter = "3";
    await this.delay(1000);
    this.countdownCounter = "2";
    await this.delay(1000);
    this.countdownCounter = "1";
    await this.delay(1000);
    this.countdownCounter = "GO!";
    await this.delay(1000);
    this.countdown = false;
  }

  joinGame(){
    var player = {
      name: this.name,
      choice: '',
      result: ''
    }

    this.gameService.joinPlayerToGame(this.gameId, player)
      .then((docRef) => this.setJoinedGame(docRef.id))
      .catch(function(error) {
          console.error("Error adding document: ", error);
      });;
    this.ngOnInit
  }

  private setJoinedGame(playerId){
    this.playerId = playerId;
    this.inGame = true;
    this.ngOnInit();
  }  

  copyToClipboard(): string{
    return environment.baseUrl + this.router.url;
  }

  leaveGame(){
    this.gameService.leaveGame(this.gameId, this.playerId)
    .then((result) => {
      console.log("leave game", result)
    });
    this.router.navigateByUrl("/");
  }

  initJoinForm(){
    this.joinForm = new FormGroup({
      name: new FormControl('', [Validators.required, Validators.maxLength(10)])
    });
  }

  private delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
  }

}