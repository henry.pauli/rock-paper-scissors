import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { GameService } from '../services/game.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  // game id
  get gameId(): string {
    return sessionStorage.getItem('gameId');;
  }
  set gameId(value: string) {
    sessionStorage.setItem('gameId', value);
  }

  // player id
  get playerId(): string {
    return sessionStorage.getItem('playerId');
  }
  set playerId(value: string) {
    sessionStorage.setItem('playerId', value);
  }

  constructor(public gameService: GameService, private router: Router) { }

  ngOnInit(): void {
    // reset storage variables
    this.gameId = "";
    this.playerId = "";
  }

  createNewGame(){
    this.gameService.createGame()
      .then((docRef)=>this.initGame(docRef.id))
      .catch(function(error) {
          console.error("Error adding document: ", error);
      });;
  }

  private initGame(gameId){
    this.router.navigate(['game', gameId]);
    sessionStorage.setItem('gameId', gameId);
  }

}
