import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { UiElementsModule } from '../ui-elements/ui-elements.module';

@NgModule({
  declarations: [HomeComponent],
  imports: [
    CommonModule,
    UiElementsModule
  ],
  exports: [
    HomeComponent
  ]
})
export class HomeModule { }
