import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { AngularFireFunctions } from '@angular/fire/functions';
import { Observable } from 'rxjs';
import { Game } from '../models/game.model';
import { Player } from '../models/player.model';

@Injectable({
  providedIn: 'root'
})
export class GameService {

  //players$: Observable<Player[]>;

  constructor(
    private firestore: AngularFirestore,
    private cloudFuctions: AngularFireFunctions,
    private http: HttpClient) { }

  createGame(){
    return this.firestore.collection("games").add({
      playersInRound: [],
      playersSelected: [],
      state: "lobby"
    });
  }

  joinPlayerToGame(gameId: string, player: Player){
    var ref = this.firestore.collection("games").doc(gameId).collection("players");
    return ref.add(player);
  }

  getPlayer(gameId: string, playerId){
    var ref = this.firestore.collection("games").doc(gameId).collection("players").doc(playerId).ref;
    return ref.get();
  }

  updatePlayer(gameId: string, playerId: string, player: Player){
    var playerDTO: Player = {
      id: player.id,
      name: player.name,
      choice: player.choice,
      result: player.result
    };

    var playerDoc = this.firestore.collection("games").doc(gameId).collection("players").doc(playerId);

    return playerDoc.update(playerDTO);
  }

  updatePlayerChoice(gameId: string, playerId: string, choice: string){
    var playerDoc = this.firestore.collection("games").doc(gameId).collection("players").doc(playerId);
    return playerDoc.update({
      choice: choice
    });
  }

  getPlayersData(gameId: string): Observable<Player[]>{
    var ref = this.firestore.collection<Game>("games").doc(gameId).collection<Player>("players");
    return ref.valueChanges();
  }

  getGameData(gameId: string): Observable<Game>{
    var ref = this.firestore.collection<Game>("games").doc(gameId);
    return ref.valueChanges();
  }

  startGame(gameId: string){
    var startGame = this.cloudFuctions.httpsCallable('initGame');
    startGame({ gameId: gameId }).toPromise()
    .then((result) => {
      console.log("start game", result)
    });
  }

  leaveGame(gameId: string, playerId: string){
    var leaveGame = this.cloudFuctions.httpsCallable('leaveGame');
    return leaveGame({gameId: gameId, playerId: playerId}).toPromise();
  }
}
