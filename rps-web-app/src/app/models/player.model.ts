export class Player {
    id?: string;
    name: string = "";
    choice: string = "";
    result: string = "";
}