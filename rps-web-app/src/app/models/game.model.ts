export class Game {
    id?: string;
    state: string = "";
    playersInRound: string[] = [];
    playersSelected: string[] = [];
}
  