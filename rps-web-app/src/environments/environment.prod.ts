export const environment = {
  production: true,
  baseUrl: "https://rock-paper-scissors-703ba.web.app",

  // For Firebase JS SDK v7.20.0 and later, measurementId is optional
  firebaseConfig : {
    apiKey: "AIzaSyBf7fM1s5QmraGBuKXRI8nN4zvA15tJ4Fg",
    authDomain: "rock-paper-scissors-703ba.firebaseapp.com",
    databaseURL: "https://rock-paper-scissors-703ba-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "rock-paper-scissors-703ba",
    storageBucket: "rock-paper-scissors-703ba.appspot.com",
    messagingSenderId: "1022677036924",
    appId: "1:1022677036924:web:3c14ae2148f5fa37513ef9",
    measurementId: "G-4Z8F8YKZTJ"
  }
};
