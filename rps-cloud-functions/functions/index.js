const functions = require("firebase-functions");
const admin = require('firebase-admin');

admin.initializeApp();
const db = admin.firestore();

exports.initGame = functions.region('europe-west1')
 .https.onCall(async (data, context) => {
    
    try {
        //var gameId = data.gameId;
        var gameRef = db.collection("games").doc(data.gameId);
        // reset game
        var playersInRound = [];
        const playerSnap = await gameRef.collection("players").get();          
        playerSnap.forEach((doc) => {
            playersInRound.push(doc.id);
        });
        await gameRef.update({
            state: "select",
            playersSelected: [],
            playersInRound: playersInRound
        });
        // reset players 
        //const playerSnap = await gameRef.collection("players").get();
        playerSnap.forEach(async(doc) => {
            var playerRef = gameRef.collection("players").doc(doc.id);
            await playerRef.update({
                choice: "",
                result: ""
            });
        });
        return {
            message: "Game started!"
        };
    }
    catch (error) {
        return {
            message: error
        };
    }
});

exports.leaveGame = functions.region('europe-west1')
 .https.onCall(async (data, context) => {
    
    try {
        //var gameId = data.gameId;
        var gameRef = db.collection("games").doc(data.gameId);
        var playerRef = db.collection("games").doc(data.gameId).collection("players").doc(data.playerId);
        // delete player doc
        await playerRef.delete();
        // reset playersInRound and playerSelected
        
        // reset game
        var playersInRound = [];
        const playerSnap = await gameRef.collection("players").get();          
        playerSnap.forEach((doc) => {
            playersInRound.push(doc.id);
        });
        await gameRef.update({
            playersInRound: playersInRound
        });
        return {
            message: "Left game."
        };
    }
    catch (error) {
        return {
            message: "Leve game error:" + error
        };
    }
});

exports.onPlayersCreate = functions.region('europe-west1').firestore
 .document('games/{gameId}/players/{playerId}')
 .onCreate(async (change, context)=>{

    try {
        var gameId = context.params.gameId;
        var playerId = context.params.playerId;
        //var data = change.after.data();
        const gameRef = db.collection("games").doc(gameId);
        //const admin = require('firebase-admin');
        const playerRef = gameRef.collection("players").doc(playerId);
        playerRef.update({
            id: playerId
        });
    } catch (error) {
        console.log(error);
    }
});

exports.onPlayersChange = functions.region('europe-west1').firestore
 .document('games/{gameId}/players/{playerId}')
 .onUpdate(async (change, context)=>{

    try {
        var gameId = context.params.gameId;
        var playerId = context.params.playerId;
        var data = change.after.data();
        const gameRef = db.collection("games").doc(gameId);
        const admin = require('firebase-admin');

        // if current choice is set
        var choiceOptions = ["rock", "paper", "scissors"];
        if(choiceOptions.includes(data.choice)){
            gameRef.update({
                playersSelected: await admin.firestore.FieldValue.arrayUnion(playerId)
            });
        }
    } catch (error) {
        console.log(error);
    }
});

exports.onGameChange = functions.region('europe-west1').firestore
 .document('games/{gameId}')
 .onWrite(async (change, context)=>{

    try {
        var data = change.after.data();
        var gameId = context.params.gameId;
        const gameRef = db.collection("games").doc(gameId);
        const playerSnap = await gameRef.collection("players").get();

        // game state select
        if(data.state == "select"){
            var playersInRoundString = data.playersInRound.sort().join(",");
            var playersSelectedString = data.playersSelected.sort().join(",");

            if(playersInRoundString === playersSelectedString){
                await gameRef.update({
                    state: "calc"
                });
            }
        }
        // game state calc
        else if(data.state == "calc"){
            //var playersInRound = data.playersInRound.sort().join(",");
            //var playersCalculated = data.playersCalculated.sort().join(",");

            var containRock = false;
            var containPaper = false;
            var containScissors = false;

            // check if draw
                // check all same
                // check all different
            
            playerSnap.forEach((doc) => {
                if(doc.data().choice == "rock"){
                    containRock = true;
                }
                else if(doc.data().choice == "paper"){
                    containPaper = true;
                }
                else if(doc.data().choice == "scissors"){
                    containScissors = true;
                }
            });

            var containAll = containRock && containPaper && containScissors;
            var containOnlyRock = containRock && !containPaper && !containScissors;
            var containOnlyPaper = !containRock && containPaper && !containScissors;
            var containOnlyScissors = !containRock && !containPaper && containScissors;
            var isDraw = containAll || containOnlyRock || containOnlyPaper || containOnlyScissors;

            if(isDraw){
                await gameRef.update({
                    state: "draw"
                });
            }
            else{
                playerSnap.forEach(async(doc) => {
                    var playerRef = gameRef.collection("players").doc(doc.id);
                    var choice = doc.data().choice;
                    var lost = false;
                    switch(choice){
                        case 'rock':
                            if(containPaper){
                                lost = true;
                            }
                            break;
                        case 'paper':
                            if(containScissors){
                                lost = true;
                            }
                            break;
                        case 'scissors':
                            if(containRock){
                                lost = true;
                            }
                            break;
                        default:
                            lost = false;
                            break;
                    }

                    // set result for player
                    if(lost){
                        await playerRef.update({
                            result: "loser"
                        });
                    }
                    else{
                        await playerRef.update({
                            result: "winner"
                        });
                    }
                });

                // set game state to result
                await gameRef.update({
                    state: "result",
                    playersSelected: []
                });
            }
        }
        // if state is draw
        else if(data.state == "draw"){
            setTimeout(async() => {
                await gameRef.update({
                    state: "select",
                    playersSelected: []
                });
                playerSnap.forEach(async(doc) => {
                    var playerRef = gameRef.collection("players").doc(doc.id);
                    await playerRef.update({
                        choice: "",
                        result: ""
                    });
                });
            }, 3000);
        }
        // if state is result
        else if(data.state == "result"){
            // check if more than one loser
            
            var losers = await gameRef.collection("players").where("result", "==", "loser").get();
            if(losers.size > 1){
                await setTimeout(async() => {

                    // reset players
                    await playerSnap.forEach(async(doc) => {
                        var playerRef = gameRef.collection("players").doc(doc.id);
                        await playerRef.update({
                            choice: "",
                            result: ""
                        });
                    });

                    var playersInRound = [];
                    await losers.forEach(async(doc) => {
                        playersInRound.push(doc.id);
                    });

                    // reset game state and players in round
                    await gameRef.update({
                        playersInRound: playersInRound,
                        state: "select",
                        playersSelected: []
                    });
                }, 3000);
            }
            else{
                await setTimeout(async() => {
                    await gameRef.update({
                        state: "lobby"
                    });
                }, 3000);
            }
        }
    }
    catch (error) {
        console.log(error);
    }
});